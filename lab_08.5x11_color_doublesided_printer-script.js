//
// Customize your print process with Print Scripting.  You don't have to be a
// programmer to use Print Scripting.  Use one of the many pre-written recipes
// already written for you, or write your own in JavaScript using snippets and
// reference documentation.
//
// Needed to capture job and run script
function printJobHook(inputs, actions) {
  
    // Needed to Analysis the print job
    if (!inputs.job.isAnalysisComplete) {
      // Full job details are not yet available. Return and wait to be called again.
      return;
    }
    
    // Adds an entry into the user's PaperCut profile.
    actions.job.addComment("This print job originated from the Student Lab's 8.5x11 Color, Double-sided, Webprint Queue.");
  }
  