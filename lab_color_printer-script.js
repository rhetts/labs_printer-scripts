//
// Last updated: 27 Dec 2019
//
// Customize your print process with Print Scripting.  You don't have to be a
// programmer to use Print Scripting.  Use one of the many pre-written recipes
// already written for you, or write your own in JavaScript using snippets and
// reference documentation.
//
// 27 Dec - Added the Allowed_IP and set the allowed to look for either IP or Client
// machine name.
//
// Needed to capture job and run script
function printJobHook(inputs, actions) {
    // your script here
    
    if (!inputs.job.isAnalysisComplete) {
      // Full job details are not yet available. Return and wait to be called again.
      return;
    }
    
    // Perform tasks here that rely on the full job details being available.
    
    // Adds the Find-Me-Printing entry point to the print job log for troubleshooting purpose.
    actions.job.addComment("This print job originated from the Student Lab's Global Color Queue.");  
    
    // Sets Name range of Helpdesk workstations.
    var Allowed_IP = 'host-47-';
    var Allowed_ClientMachine = 'ATS-HD-COMP';
    var allowed = inputs.job.clientMachineOrIP.includes(Allowed_IP || Allowed_ClientMachine);
    
    // Test if a user is sitting at a helpdesk workstation.
    if (allowed !=0) {
      // Changes the name of the document that is logged in the database.
      actions.job.changeDocumentName('Helpdesk Print Job');
      // Sets job costs to 0 as this is a work related print job, not subject to student quotas
      actions.job.setCost(0);
      //By-passes the hold in the release queue
      actions.job.bypassReleaseQueue();
      //Redirects print job to specific print queue
      actions.job.redirect("cloudprint\\oit_labprinter_6R6596", {allowHoldAtTarget: false, recalculateCost: false });
      // Add comments to the logs
      actions.log.info('The user "' + inputs.job.username + '" is logged in to helpdesk workstation "' + inputs.job.clientMachineOrIP + '".  Bypassing release queue for this helpdesk print job and setting the costs to 0.');
    }
  }
  